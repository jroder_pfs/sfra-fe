import React, { Component } from 'react';
import './App.scss';
import { connect } from 'react-redux';
import { hot } from 'react-hot-loader';
import PropTypes from 'prop-types';

/**
 * Pulls cssModules data defined in htmlHead.isml, 
 *  adds them as imports here
 */
if (typeof window !== 'undefined' && window.cssModules) {
  window.cssModules.forEach( cssModule => {
    var scssModule = '/' + cssModule;
    console.log('importing', scssModule);

    // Secret Sauce
    // We need to do a webpack dynamic import. This call will build 
    //  a module of every .scss file found in the furthest-down static 
    //  folder you define (/scss/ in this case). The problem with this is 
    //  that SFRA's scss setup uses partials that depend on variables that 
    //  don't exist in the standalone files. So to skip them we need to use 
    //  the 'webpackExclude' magic comment with a regex that matches 
    //  filenames that start with an underscore. This will stop 
    //  webpack from processing standalone modules on the partials.
    //
    // This will not work without @babel/plugin-syntax-dynamic-import

    import(
        /* webpackExclude: /.*_[a-zA-Z0-9_]*\.scss$/ */
      `../../cartridges/app_storefront_base/cartridge/client/default/scss${scssModule}.scss`
      );
  })
}

class App extends Component {
  render() {
    const { count, dispatch } = this.props;
    return (
      <div className="App__client">
        <h2>React client running</h2>
        <label>(localhost:3000/bundle.js)</label>
        <hr />
        <h2>Features</h2>
        <ul>
          <li>SCSS Hot Modules</li>
        </ul>

        <h2>Imports</h2>
        <ul>
        {window.cssModules.map( (cssModule, i) => {
          return <li key={'feature-' + i}>{cssModule}.scss</li>
        })}
        </ul>
      </div>
    );
  }
}

App.propTypes = {
  count: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired
};

export default hot(module)( connect(state => ({count: state}))(App) );