const fs = require("fs");
import {packageName} from './package.json';

/**
 * Settings to pass between all custom webpack localdev processes
 */
export default {
  // link this to the existing cartridge in package.json (SFRA standard)
  cartridge: packageName,
  server: {
    protocol: 'http',
    host: 'localhost',
    port: 3000,
  },

  getCartridgePath: function() {
    return `cartridges/${this.cartridge}/cartridge/`
  },
  
  // checks if the currently set cartridge exists
  cartridgeExists: function() {
    return fs.existsSync(this.getCartridgePath());
  },
  
  getHost: function() {
    return `${this.server.protocol}://${this.server.host}:${this.server.port}/`;
  }
};
