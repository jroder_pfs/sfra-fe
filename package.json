{
  "name": "sfra",
  "version": "4.4.1",
  "description": "Storefront Reference Architecture",
  "main": "index.js",
  "engines": {
    "node": ">=4.0 <12.0.0"
  },
  "scripts": {
    "test": "sgmf-scripts --test test/unit/**/*.js",
    "cover": "sgmf-scripts --cover 'test/unit'",
    "test:integration": "sgmf-scripts --integration 'test/integration/**/*.js'",
    "test:acceptance:custom": "npx codeceptjs run --plugins retryFailedStep --profile",
    "test:acceptance:deep": "npx codeceptjs run --plugins retryFailedStep --grep '(?=.*)^(?!.*@mobile)^(?!.*@tablet)^(?!.*@pageDesigner)' --profile",
    "test:acceptance:smoke": "npx codeceptjs run --plugins retryFailedStep --grep @happyPath --profile",
    "test:acceptance:pagedesigner": "npx codeceptjs run --plugins retryFailedStep --grep @pageDesigner --profile",
    "test:acceptance:desktop": "npx codeceptjs run --plugins retryFailedStep --grep '(?=.*)^(?!.*@mobile)^(?!.*@tablet)^(?!.*@pageDesigner)^(?!.*@deepTest)' --profile",
    "test:acceptance:mobile": "npx codeceptjs run --plugins retryFailedStep --profile sauce:phone --grep @mobile",
    "test:acceptance:tablet": "npx codeceptjs run --plugins retryFailedStep --profile sauce:tablet --grep @tablet",
    "test:acceptance:parallel": "npx codeceptjs run-multiple parallel --plugins retryFailedStep --profile",
    "test:acceptance:multibrowsers": "npx codeceptjs run-multiple multibrowsers --plugins retryFailedStep --profile",
    "test:acceptance:report": "./node_modules/.bin/allure serve test/acceptance/report",
    "bdd:snippets": "./node_modules/.bin/codeceptjs bdd:snippets --path",
    "compile:scss": "sgmf-scripts --compile css",
    "compile:js": "sgmf-scripts --compile js",
    "compile:fonts": "node bin/Makefile compileFonts",
    "build": "npm run compile:js & npm run compile:fonts & npm run compile:scss",
    "lint": "npm run lint:css && npm run lint:js",
    "lint:css": "sgmf-scripts --lint css",
    "lint:js": "sgmf-scripts --lint js",
    "upload": "sgmf-scripts --upload",
    "uploadCartridge": "sgmf-scripts --uploadCartridge app_storefront_base && sgmf-scripts --uploadCartridge modules && sgmf-scripts --uploadCartridge bm_app_storefront_base",
    "watch": "sgmf-scripts --watch",
    "watch:static": "sgmf-scripts --watch static",
    "release": "node bin/Makefile release --",
    "develop": "babel-node server.js --env development",
    "develop:debug": "babel-node --inspect server.js --env development"
  },
  "repository": {
    "type": "git",
    "url": "git+https://github.com/SalesforceCommerceCloud/storefront-reference-architecture.git"
  },
  "author": "Ilya Volodin <ivolodin@demandware.com>",
  "license": "ISC",
  "homepage": "https://github.com/SalesforceCommerceCloud/storefront-reference-architecture",
  "devDependencies": {
    "@babel/cli": "^7.8.3",
    "@babel/core": "^7.6.3",
    "@babel/node": "^7.8.3",
    "@babel/plugin-proposal-object-rest-spread": "^7.6.2",
    "@babel/plugin-syntax-dynamic-import": "^7.8.3",
    "@babel/plugin-transform-runtime": "^7.8.3",
    "@babel/preset-env": "^7.6.3",
    "@babel/preset-react": "^7.8.3",
    "@tridnguyen/config": "^2.3.1",
    "@wdio/sauce-service": "^5.14.0",
    "@wdio/selenium-standalone-service": "^5.13.2",
    "acorn": "^7.1.0",
    "allure-commandline": "^2.13.0",
    "appium": "^1.15.0",
    "autoprefixer": "^9.7.3",
    "babel-core": "^6.26.3",
    "babel-eslint": "^8.2.6",
    "babel-loader": "^8.0.6",
    "babel-plugin-dynamic-import-node": "^2.3.0",
    "babel-plugin-react-transform": "^3.0.0",
    "babel-plugin-transform-runtime": "^6.23.0",
    "babel-preset-env": "^1.7.0",
    "babel-preset-react": "^6.24.1",
    "babel-register": "^6.26.0",
    "babel-runtime": "^6.26.0",
    "chai": "^3.5.0",
    "chai-subset": "^1.6.0",
    "chalk": "^1.1.3",
    "cheerio": "0.22.0",
    "chokidar": "^2.1.8",
    "cldr-data": "^32.0.1",
    "codeceptjs": "^2.3.2",
    "codeceptjs-cucumber": "^2.0.10",
    "codeceptjs-saucehelper": "^1.4.0",
    "codeceptjs-saucelabs": "^2.0.6",
    "codeceptjs-shared": "^2.0.6",
    "cors": "^2.8.5",
    "css-loader": "^0.28.11",
    "css-modules-require-hook": "^4.2.3",
    "debug": "^4.1.1",
    "deepmerge": "^3.3.0",
    "dotenv-webpack": "^1.7.0",
    "eslint": "^3.19.0",
    "eslint-config-airbnb-base": "^5.0.3",
    "eslint-plugin-import": "^1.16.0",
    "eslint-plugin-react": "^7.18.0",
    "eslint-plugin-sitegenesis": "~1.0.0",
    "express": "^4.17.1",
    "extract-text-webpack-plugin": "^4.0.0-beta.0",
    "extracted-loader": "^1.0.7",
    "globalize": "^1.4.2",
    "istanbul": "^0.4.5",
    "lodash": "^4.17.15",
    "minimist": "^1.2.0",
    "mocha": "^5.2.0",
    "mocha-junit-reporter": "^1.23.1",
    "moment-timezone": "^0.5.26",
    "node-sass": "^4.12.0",
    "postcss-loader": "^3.0.0",
    "postcss-url": "^8.0.0",
    "prop-types": "^15.7.2",
    "properties-parser": "^0.3.1",
    "proxyquire": "1.7.4",
    "react": "^16.12.0",
    "react-dom": "^16.12.0",
    "react-hot-loader": "^4.12.18",
    "react-redux": "^5.1.2",
    "redux": "^4.0.5",
    "request-promise": "^4.2.4",
    "sass-loader": "^7.3.1",
    "selenium-standalone": "^6.16.0",
    "sgmf-scripts": "^2.3.0",
    "shelljs": "^0.8.3",
    "should": "^13.2.3",
    "sinon": "^1.17.7",
    "style-loader": "^0.21.0",
    "stylelint": "^8.4.0",
    "stylelint-config-standard": "^17.0.0",
    "stylelint-scss": "^2.5.0",
    "url": "^0.11.0",
    "url-loader": "^3.0.0",
    "wdio-mocha-framework": "^0.6.4",
    "wdio-sauce-service": "^0.4.14",
    "webdriverio": "^5.14.5",
    "webpack": "^4.41.5",
    "webpack-bundle-analyzer": "^3.6.0",
    "webpack-cli": "^3.3.10",
    "webpack-command": "^0.5.0",
    "webpack-dev-middleware": "^3.7.2",
    "webpack-hot-middleware": "^2.25.0",
    "webpack-log": "^3.0.1",
    "webpack-node-externals": "^1.7.2",
    "xml2js": "^0.4.22"
  },
  "dependencies": {
    "bootstrap": "4.3.1",
    "cleave.js": "^1.5.3",
    "flag-icon-css": "^2.9.0",
    "font-awesome": "^4.7.0",
    "jquery": "^3.4.1"
  },
  "browserslist": [
    "last 2 versions",
    "ie >= 10"
  ],
  "packageName": "app_storefront_base",
  "babel": {
    "presets": [
      "@babel/preset-react",
      [
        "@babel/preset-env"
      ]
    ],
    "plugins": [
      "@babel/plugin-transform-runtime",
      "@babel/plugin-syntax-dynamic-import"
    ]
  }
}
