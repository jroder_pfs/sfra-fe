'use strict';

var URLUtils = require('dw/web/URLUtils');

var styles = [];
var scripts = [];

// keep versions of asset URLs relative to localhost (local dev mode)
var localStyles = [];
var localScripts = [];

/**
 * If the resource to add is not already in the resource array then add it to the array
 * @param {Array} resourceArray - Either the scripts or styles array to which you want to add the resource src to.
 * @param {string} src - URI of the resource to add
 * @param {string} integrity - cryptographic hash of a resource
 */
function addResource(resourceArray, src, integrity) {
    var result = {};
    var exists = resourceArray.some(function (element) {
        return element.src === src;
    });

    if (!exists) {
        result.src = src;
        if (integrity) {
            result.integrity = integrity;
        }

        resourceArray.push(result);
    }
}

/**
 * Transforms path to CSS asset for localhost development.
 * @param {string} src  url to asset with filename and extension (ext optional)
 */
function devModeStyleUrl (src) {
    return src
        .replace(/\/css\//, '') // remove parent css dir
        .replace(/\.css$/, ''); // remove .css ext
}

module.exports = {
    addCss: function (src, integrity) {
        // Localdev custom code: add the localhost url to a separate array so that 
        // when the DW var 'isDevelopmentMode' is set to true in htmlHead.isml
        addResource(localStyles, devModeStyleUrl(src), integrity);
        
        if (/((http(s)?:)?\/\/).*.css/.test(src)) {
            addResource(styles, src, integrity);
        } else {
            addResource(styles, URLUtils.staticURL(src).toString(), integrity);
        }
    },
    addJs: function (src, integrity) {
        // Localdev custom code: add the localhost url to a separate array so that 
        // when the DW var 'isDevelopmentMode' is set to true in htmlHead.isml
        addResource(localScripts, URLUtils.staticURL(src).toString(), integrity);
        
        if (/((http(s)?:)?\/\/).*.js/.test(src)) {
            addResource(scripts, src, integrity);
        } else {
            addResource(scripts, URLUtils.staticURL(src).toString(), integrity);
        }
    },
    scripts: scripts,
    styles: styles,
    // custom variables to store the localhost urls for assets
    localScripts: localScripts,
    localStyles: localStyles

};
