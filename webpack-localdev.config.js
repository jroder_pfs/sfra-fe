import webpack from 'webpack';
import path from 'path';
import AppSettings from './localdev.config';
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

process.env.NODE_ENV = process.env.NODE_ENV || "development";

const host = AppSettings.getHost();
const cartridge = AppSettings.cartridge;

console.info('-------------------------------------');
console.log('webpack-localdev.config.js START');

if(!AppSettings.cartridgeExists()) {
  console.error(`CONFIG ERROR: cartridge '${AppSettings.cartridge}' not found`);
  process.exit();
} else {
  console.info('Cartridge passed existence check');
}
console.info('-------------------------------------');

console.log('');
console.log('Host address: ', host);
console.log('Cartridge: ', cartridge);
console.log('');

const scssPathPattern = new RegExp(`.*${cartridge}/cartridge/client/.*/scss`);

module.exports = (env) => {
  const isDevelopment = env === 'development';
  const config = {};
  var AnalyzerPlugin;

  if(isDevelopment) {
    config.mode = 'development';
    config.devtool = 'eval-source-map';
    AnalyzerPlugin = new BundleAnalyzerPlugin ({analyzerMode: 'static'});
  } else {
    config.mode = 'production';
    config.devtool = 'source-map';
    AnalyzerPlugin = new BundleAnalyzerPlugin ({analyzerMode: 'disabled'});
  }

  config.entry = [
    `webpack-hot-middleware/client?path=${host}__webpack_hmr`,
    './client/app.js'
  ];

  config.output = {
    path: __dirname,
    filename: 'bundle.js',
    publicPath: host
  };

  config.plugins = [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    AnalyzerPlugin
  ];

  config.resolve = {
    alias: {
      request: 'browser-request'
    }
  };

  config.module = {
    rules: [
      // Javascript
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: path.join(__dirname, 'client'),
        query: {
          "env": {
            "development": {
              "plugins": ["react-hot-loader/babel"],
            }
          },
        }
      },

      // S/CSS
      {
        test: /\.(s)css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              url: false,
              modules: false
            } 
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: [
                require('autoprefixer')(),
                require('postcss-url')({
                  url: function (asset, dir) {
                    /*
                      SECRET SAUCE
                      We need to remap url() paths in the SCSS as the expected
                      output directory is outside the client/scss structure. 

                      In order to do this, we test two conditionals: if the SCSS
                      file containing the URL is inside a client/scss parent inside 
                      the currently set cartridge, and if the url to the asset is a 
                      relative ../images. If both of these conditions are true, we 
                      will remap the URL to host/cartdrige/static (Express static 
                      route set in server.js)
                    */
                    var isStaticImagePath = !!asset.url.match(/\.\.\/images\//);
                    var isScssPath = !!dir.from.match(scssPathPattern);

                    if (isStaticImagePath && isScssPath) {
                      var baseTemplate = `${host}static/%VERSION%/`,
                        split = dir.from.split('/'),
                        // Language folder name
                        version = split[split.indexOf('scss') - 1],
                        // File relative path + filename
                        filepath = asset.url.replace(/.*\.\.\/images\//, ''),
                        transformed = baseTemplate.replace(/\%VERSION\%/, version) + 'images/' + filepath;
                      
                      console.log('postscss-url()       asset  | ', asset.url);
                      // console.log('postscss-url()    filename  | ', filename);
                      console.log('postscss-url()        from  | ', dir.from);
                      // console.log('postscss-url()          to  | ', dir.to);
                      console.log('postscss-url()     version  | ', version);
                      console.log('postscss-url()    filepath  | ', filepath);
                      console.log('postscss-url() transformed  | ', transformed);
                      console.log('');

                      return transformed;
                    }

                  }
                })
              ]
            }
          },
          {
            loader: 'sass-loader',
            options: {
              modules: false,
              // sourceMap: true,
              // sourceMapContents: false
            }
          }
        ]
      },
      
      /* { 
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, 
        loader: "url-loader?limit=10000&mimetype=application/font-woff" 
      },
      { 
        test: /\.(jpe?g|gif|png|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, 
        loader: "file-loader" 
      }, */
    ]
  };

  return config;
};